# SSH scripts for connecting to mistral.dkrz.de

These scripts help you to establish a connection to vncserver or
Jupyter Notebook running on a mistral node. The scripts handle job submission,
tunnel set-up, and start the client application on your local system.

As bash scripts they run natively on Linux, MacOS, and other Unix-like
systems. After installing WSL (Windows Subsystem for Linux), they also run
on Microsoft Windows 10.

## start-jupyter

The script is being modified to allow further features:

* connecting to different DKRZ systems: mistral, vader, levante (?)
* start jupyter notebooks in singularity containers 

